**TEAM HISTORY:**
**This Document will attempt to summarize and describe all the work we did as a team throughout the semester in hopes it will benefit the future team working on this project.

**Sprint 1**
One of the first things we did as a team was create a basic understanding of Keycloak for ourselves. We needed to know exactly what it is that we are implementing into our Microservice Architecture and how we were going to do it.

**What is Keycloak?**
- Keycloak is a tool for Identity Management and Access System. Picture any website you have with a login system. You would assume somewhere on the site there is somewhere a user can type their Username, Password, and click the Login button. Keycloak is our tool that authenticates that user. It also depicts what permissions this user has. 
- In our case, Keycloak be used to setup these permissions. We don't want a customer to be able to manipulate our databases. On the other hand, we will want WSU Employees at have permission to manipulate our databases. But not complete control. The Admins will have complete control. Ideally this will be you, the programmer. 

**Features:**

- Single Sign-on: and Single Sign-out
- Standard Protocols: OAuth 2.0, OIDC 1.0. SAML 2.0
- Flexible Authentication and Authorization
- Multi-factor Authentication: One- Time Passwords
 Social Login: Google, Facebook, Twitter,….
- Provides centralized User Management.
- Supports Directory Services.
 Customizable and Extensible
- Easy Setup and Integration.

**Booting Keycloak in Standalone Mode:** [**https://www.keycloak.org/getting-started/getting-started-zip**](https://www.keycloak.org/getting-started/getting-started-zip)

- This tutorial comes straight from Keycloak Docs. This is how you boot up Keycloak locally on your system. This requires no Coding. This is just to show you what Keycloak is, how to launch it, and how to configure realms, clients, users, and roles.

Although Keycloak is our team's main focus, time was also spent on learning about these tools during our first Sprint. 
- Kubernetes
- RabbitMQ
- Gateway/Ingress

Kubernetes is cluster and container management tool. RabbitMQ is a message Broker. And Ingress exposes HTTP and HTTPS routes from outside the cluster to services within the cluser. 

It may sound extremely foriegn at first, but it is actually extremely similar to things you have been doing in previous classes. Essentially, these are the tools we will use to communicate with software written by other teams. These tools are associated with setting up a application that works on a network. 

 

**Sprint 2**
- Frontend: Vue.js
- Backend: Node.js

**Sprint 3**
Deploying Keycloak on a Kubernetes Cluster
